import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (props) {



    return (
        <Box sx={{ width:"60%" }}>
            <Paper elevation={4}>
                
                <h2> จัดทำโดย : { props.name }</h2>
                <h3> สาขา : { props.branch }</h3>
                <h3> ติดต่อได้ที่ : { props.email }</h3>
                <h3> ติดต่อได้ที่ : { props.tel }</h3>
                <br />
                </Paper>
        </Box>
    );
}

export default AboutUs;