
import GradeResult from '../components/GradeResult';
import { useState } from "react";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from '@mui/material/';


function GradeCalPage() {

    const [ name, setName ] = useState("");
    const [ score, setScore ] = useState("");
    const [ gradeResult, setGradeResult ] = useState(0);
    const [ translateResult, setTranslateResult ] = useState("");

    function calculateGrade()
        {
        setGradeResult( gradeResult );    
        var gradeResult = document.getElementById("gradeResult").value;
        if(setGradeResult >= 80)
            setTranslateResult("ได้เกรด A")
        else if(setGradeResult >= 70)
            setTranslateResult("ได้เกรด B")
        else if(setGradeResult >= 60)
            setTranslateResult("ได้เกรด C")
        else if(setGradeResult >= 50)
            setTranslateResult("ได้เกรด D")
        else
            setTranslateResult("ได้เกรด E")
        }

    return (
        <Container maxWidth='lg'>
            <Grid container spacing={2} sx={{ marginTop : "10px" }}>
                <Grid item xs={12}>
                    <Typography variant="h5">
                        ยินดีต้อนรับสู่เว็บคำนวณ GRADE
                    </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign : 'left'}} >
                        คุณชื่อ : <input type="text"
                                    value={name} 
                                    onChange={ (e) => { setName(e.target.value); } } />  
                                    <br />
                                    <br />
                        คะแนนของคุณ : <input type="text" 
                                    value={score} 
                                    onChange={ (e) => { setScore(e.target.value); } } />  
                                    <br />
                                    <br />

                        <Button variant="contained" onClick={ ()=>{ calculateGrade() } }>
                            Calculate
                        </Button>
                    </Box>

                </Grid>
                <Grid item xs={4}>
                    { gradeResult != 0 &&  
                        <div>
                            <hr />
                            ผลการคำนวณเกรดของคุณ
                            <GradeResult 
                                name={ name }
                                score = { score }
                                gradeResult = { gradeResult }
                            />
                        </div>
                    }
                </Grid>
            </Grid>            
        </Container>

    );
}

export default GradeCalPage;

